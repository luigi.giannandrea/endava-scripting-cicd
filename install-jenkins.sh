#!/bin/bash
echo -------------------------------------------------------------
echo Endava - School of Devops - Scripting - CICD - Git
echo Luigi Giannandrea
echo -------------------------------------------------------------
echo install-jenkins.sh - Check for dependencies and installation
echo -------------------------------------------------------------

echo -e "\nINFO: Checking for Jenkins and Assignment prerequisites..."

dpkg -l openjdk-11-jre 2> /dev/null 1>/dev/null

if [ "$?" -eq 0 ] 
then
	echo INFO: Java 11 is installed...
else
	echo INFO: Java 11 is not installed...
	echo -e "\nDo you want to install it? (Y)es / (N)o"
	read l;
	if [ "$l" = "Y" ] || [ "$l" = "y" ]
	then
		echo "INFO: Trying to install Java. Please wait..."
		sudo apt install -y openjdk-11-jre > /dev/null 2> /dev/null
	else
		echo "ERROR: Aborting Jenkins installation due to missing Java package installed."
		exit -1
	fi
fi

dpkg -l nodejs 2> /dev/null 1>/dev/null
if [ "$?" -eq 0 ]
then
	echo INFO: npm is installed...
else
	echo INFO: npm is not installed...
	echo -e "\n Do you want to install it? (Y)es / (N)o"
	read l;
	if [ "$l" = "Y" ] || [ "$l" = "y" ]
	then
		echo "INFO: Trying to install npm. Please wait..."
		curl -fsSL https://deb.nodesource.com/setup_18.x | sudo bash - 2>/dev/null 1>/dev/null
		sudo apt-get install -y nodejs 2>/dev/null 1>/dev/null
	else
		echo "ERROR: Aborting the script. Npm is required for the assignment"
		exit -2
	fi
fi




dpkg -l git 2> /dev/null 1>/dev/null

if [ "$?" -eq 0 ] 
then
	echo INFO: Git is installed...
else
	echo INFO: Git is not installed...
	echo -e "\nDo you want to install it? (Y)es / (N)o"
	read l;
	if [ "$l" = "Y" ] || [ "$l" = "y" ]
	then
		echo "INFO: Trying to install Git. Please wait..."
		sudo apt install -y git > /dev/null 2> /dev/null
	else
		echo "ERROR: Aborting Jenkins installation since we need Git to clone the app repo."
		exit 1
	fi
fi



echo "INFO: Checking if Jenkins' Repository Public Key is already provisioned..."
if [ -f /usr/share/keyrings/jenkins-keyring.asc  ]
then
	echo "INFO: Public key found..."
else
	echo "INFO: The Jenkins public key is not provisioned yet."
	echo -e "\nDo you want to provision it? (Y)es / (N)o"
	read l;
	if [ "$l" = "Y" ] || [ "$l" = "y" ]
	then
		
		echo "INFO: Creating Public Key at /usr/share/keyrings/jenkins-keyring.asc"
		curl -fsSL https://pkg.jenkins.io/debian/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
	else
		echo "ERROR: Aborting Jenkins installation due to missing repository's public key"
		exit 2
	fi
fi

if [ -f /etc/apt/sources.list.d/jenkins.list ]
then
	echo "INFO: Jenkins repository for debian exists..."
else
	echo "INFO: The Jenkins repository does not exists yet..."
	echo -e "\nDo you want to create it? (Y)es / (N)o"
	read l;
	if [ "$l" = "Y" ] || [ "$l" = "y" ]
	then
		echo "INFO: Creating Jenkins repository definition in /etc/apt/sources.list.d/jenkins.list"
		echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null
	else
		echo "ERROR: Aborting Jenkins installation due to missing repository definition..."
		exit 3
	fi
fi


echo "INFO: Preparing to install Jenkins. Please wait..."
sudo apt update > /dev/null 2>/dev/null

if [ $? -eq 0 ]
then
	echo INFO: Apt repository updated successfully...
	echo INFO: Trying to install Jenkins packages. Please wait...
	sudo apt-get install -y jenkins > /dev/null 2> /dev/null
	if [ $? -eq 0  ]
	then
		echo "INFO: Jenkins installed successfully"
		echo -e "\nINFO: Your Jenkins initial password is: $(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)"
	else
		echo "ERROR: There was an error installing Jenkins."
		echo "ERROR: Please check your internet connection or anything avoiding the installation."
	fi
else
	echo ERROR: Error updating the repository.
	echo ERROR: Please check your internet connection or anything avoiding the update.
	exit 4
fi

